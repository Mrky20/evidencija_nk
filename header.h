#ifndef HEADER_H
#define HEADER_H

typedef struct datumrodjenja {
	int dan;
	int mjesec;
	int godina;
}DATUMRODJENJA;
typedef struct unosigraca {
	char ime[30];
	char prezime[30];
	DATUMRODJENJA datumRodjenja;
	char pozicija[30];
	int brIskaznice;
	int placa;
	char status[15];
}UNOSIGRACA;
void unosIgraca();
void pregledPopisa();
int izlaz();
void pretragaIgraca();
#endif //HEADER_H