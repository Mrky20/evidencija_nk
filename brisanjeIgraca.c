#define _CRT_SECURE_NO_WARNINGS
#include "header.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

void brisanjeIgraca() {
	int br;
	FILE* fpbr1 = NULL;
	fpbr1 = fopen("ukupanBr.txt", "r");
	fscanf(fpbr1, "%d", &br);
	fclose(fpbr1);
	UNOSIGRACA* igrac = NULL;
	igrac = (UNOSIGRACA*)malloc(br*sizeof(UNOSIGRACA));
	FILE* fp;
	fp = fopen("unosIgraca.bin", "rb");
	if (fp==NULL) {
		printf("Datoteka ne postoji na disku (popis nije stvoren). Kako bi ste stvorili popis odaberite funkciju pod rednim brojem '1' na glavnom izborniku kako biste stvorili datoteku i stvorili popis.");
		printf("\n1. Povratak u glavni izbornik\n2. Izlaz iz aplikacije\n");
		int povratak;
		scanf("%d", &povratak);
		if (povratak == 1) {
		}
		else if (povratak == 2) {
			izlaz();
		}
		
	}
	else {
		fp = fopen("unosIgraca.bin", "rb");
		for (int i = 0; i < br; i++) {
				fread(&igrac[i], sizeof(UNOSIGRACA), 1, fp);
			}
		fclose(fp);
		remove("unosIgraca.bin");
		FILE* fpbr2 = NULL;
		fpbr2 = fopen("unosIgraca.bin", "wb");
		printf("Unesite broj iskaznice igraca kojeg zelite obrisati:");
		int brIs;
		int provjeraIs = 0;
		do {
		scanf("%d", &brIs);
		for (int i = 0; i < br; i++) {
			if (igrac[i].brIskaznice == brIs)
				provjeraIs = 1;
		}
		if (provjeraIs == 0) {
			printf("Igrać sa unesenim brojem iskaznice ne postoji. \nMolim ponovno unesite ispravan broj iskaznice:\n");
		}

		} while (provjeraIs == 0);
		
		for (int i = 0; i < br; i++) {
			if (igrac[i].brIskaznice != brIs) {
				fwrite(&igrac[i], sizeof(UNOSIGRACA), 1, fp);
			}
			else {
			}
		}
		fclose(fpbr2);
		br--;
		fpbr1 = fopen("ukupanBr.txt", "w");
		fprintf(fpbr1, "%d", br);
		fclose(fpbr1);
		printf("\n1. Povratak u glavni izbornik\n2. Izlaz iz aplikacije\n");
		int povratak;
		printf("Odaberite funkciju koju zelite koristiti: ");
		scanf("%d", &povratak);
		if (povratak == 1) {
		}
		else if (povratak == 2) {
			izlaz();
		}
	}
	
}