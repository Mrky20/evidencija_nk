#define _CRT_SECURE_NO_WARNINGS
#include "header.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

void unosIgraca() {
	int ukupanBr = 0;
	UNOSIGRACA* igrac = NULL;
	int brIgraca;
	FILE* fpbr1 = NULL;
	fpbr1 = fopen("ukupanBr.txt", "r");
	if (fpbr1 == NULL) {
		printf("\n(Datoteka za pohranjivanje ukupnog broja unesenih igraca je stvorena)\n");
		FILE* fpbr = fopen("ukupanBr.txt", "w+");
		int s = 0;
		fprintf(fpbr, "%d",s);
		fclose(fpbr);
	}
	else {
		fscanf(fpbr1, "%d", &ukupanBr);
		fclose(fpbr1);
	}
	printf("\nUnesite broj koliko igraca zelite unijeti: ");
	scanf("%d", &brIgraca);
	igrac = (UNOSIGRACA*)malloc(brIgraca * sizeof(UNOSIGRACA));
	FILE* fp = NULL;
	fp = fopen("unosIgraca.bin", "a+b");
	if (fp == NULL) {
		fprintf(stderr, "Vrijednost pogreske je: %d.\n", errno);
		fprintf(stderr, "Pogreska: %s.\n", strerror(errno));
		return 0;
	}
	else {
		for (int i = 0; i < brIgraca; i++) {
			printf("\nUnesite podatke koje zelite pohraniti za igraca: \n");
			printf("\nIme: ");
			scanf("%s", igrac[i].ime);
			printf("Prezime: ");
			scanf("%s", igrac[i].prezime);
			printf("Datum rodjenja: ");
			scanf("%d.%d.%d.", &igrac[i].datumRodjenja.dan, &igrac[i].datumRodjenja.mjesec, &igrac[i].datumRodjenja.godina);
			printf("Pozicija: ");
			scanf("%s", igrac[i].pozicija);
			printf("Broj iskaznice: ");
			scanf("%d", &igrac[i].brIskaznice);
			printf("Placa: ");
			scanf("%d", &igrac[i].placa);
			printf("Status: ");
			scanf("%s", igrac[i].status);
			fwrite(&igrac[i], sizeof(UNOSIGRACA), 1, fp);
			ukupanBr++;
		}
		fclose(fp);
		free(igrac);
		fpbr1 = fopen("ukupanBr.txt", "w");
		fprintf(fpbr1, "%d", ukupanBr);	
		fclose(fpbr1);
		printf("\n1. Povratak u glavni izbornik\n2. Izlaz iz aplikacije\n");
		int povratak;
		printf("Odaberite opciju koju zelite koristiti: ");
		scanf("%d", &povratak);
		if (povratak == 1) {
		}
		else if(povratak==2){
			izlaz();
		}
	}
}