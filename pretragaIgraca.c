#include "header.h"
#include <stdio.h>
#include <stdlib.h>

void pretragaIgraca() {
	int br;
	FILE* fpbr1 = NULL;
	fpbr1 = fopen("ukupanBr.txt", "r");
	fscanf(fpbr1, "%d", &br);
	fclose(fpbr1);
	int brIs;
	printf("\nUnesite broj iskaznice igraca kojeg zelite pronaci: ");
	scanf("%d", &brIs);
	UNOSIGRACA* igrac = NULL;
	igrac = (UNOSIGRACA*)malloc(sizeof(UNOSIGRACA));
	FILE* fp = NULL;
	fp = fopen("unosIgraca.bin", "rb");
	if (fp == NULL) {
		printf("Datoteka ne postoji na disku (popis ne postoji). Kako bi ste stvorili popis odaberite broj '1' na glavnom izborniku kako biste stvorili datoteku i stvorili popis.");
	}
	
	else {
		int provjera = 0;
		for (int i = 0; i < br; i++) {
			fread(igrac, sizeof(UNOSIGRACA), 1, fp);
			if (igrac->brIskaznice == brIs) {
				printf("\nIme:\t\t %s\nPrezime:\t %s\nDatum rodjenja:\t %d.%d.%d.\nPozicija:\t %s\nBroj iskaznice:\t %d\nPlaca:\t\t %d\nStatus:\t\t %s\n\n\n\n", igrac->ime, igrac->prezime, igrac->datumRodjenja.dan, igrac->datumRodjenja.mjesec, igrac->datumRodjenja.godina, igrac->pozicija, igrac->brIskaznice, igrac->placa, igrac->status);
				provjera = 1;
			}
		}
		if(provjera=1)
			printf("\nIgrac s tim brojem iskaznice ne nalazi se u popisu.");
		fclose(fp);
		free(igrac);

	}
	printf("\n1. Povratak u glavni izbornik\n2. Izlaz iz aplikacije\n");
	int povratak;
	printf("Odaberite opciju koju zelite koristiti: ");
	scanf("%d", &povratak);
	if (povratak == 1) {
	}
	else if (povratak == 2) {
		izlaz();
	}
}