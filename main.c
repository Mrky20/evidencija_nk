#define _CRT_SECURE_NO_WARNINGS
#include "header.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main() {
	int brIgraca=0;
	int a;
	do{
		printf("\n\tEVIDNECIJA IGRACA NOGOMETNOG KLUBA\n");
		printf("\t\tDobrodosli! \n");
		printf("\nUnesite redni broj pored opcije koju zelite koristiti: \n");
		printf("\n(Prilikom prvog pokretanja programa¨morate stvoriti popis odaberite opciju pod rednim brojem '1')\n");
		printf("\n1. Unos novog igraca\n2. Pregled popisa\n3. Brisanje igraca\n4. Pretraga igraca\n5. Izlaz iz aplikacije\n");
		printf("\nOdabir: ");
		do{
			scanf("%d", &a);
			if (a < 1 || a>5) {
				printf("Neispravan unos, molim unesite broj koji odgovara rednom broju jednoj od funkcija: ");
			}
		} while (a < 1 || a>5);
		if (a == 1) {
			unosIgraca();	
		}else if (a == 2) {
			pregledPopisa();
		}else if (a == 3) {
			brisanjeIgraca();
		}else if (a == 4) {
			pretragaIgraca();
		}
		else if (a == 5) {
			izlaz();
		}
	} while (1);
}